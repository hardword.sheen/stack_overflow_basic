This is my personal repo to understand the basics of stack overflow.

Codes are copied from:
- https://ctf-wiki.github.io/ctf-wiki/pwn/linux/stackoverflow/stackoverflow-basic/
- https://dhavalkapil.com/blogs/Buffer-Overflow-Exploit/

Compile in Kali(64bit)

`gcc -m32 -fno-stack-protector -z execstack -no-pie -Wl,-z,norelro -static -o {source} {source.c}`

Need `pwntools` python module to run exploit.py
- http://docs.pwntools.com/