#include <stdio.h>
#include <string.h>

void key() { puts("You Hava already controlled it.\nFlag{4cb1bed41f6b975627847554b7f82290}"); }

void vulnerable() {
  char s[12];
  gets(s);
  puts(s);
  return;
}

int main(int argc, char **argv) {
  vulnerable();
  return 0;
}
