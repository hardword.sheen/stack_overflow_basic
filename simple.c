#include <stdio.h>

int func(int a, int b)
{
    printf("%d\n", a + b); 
    return 0;
}
void main()
{
    func(1, 2);
    // next instruction
    return;  
}
